@extends('layouts.principal')

@section('content')

<div class="review-content">
			<div class="top-header span_top">
						<div class="top-header">
				<div class="logo">
					<div class="row">
						<div class="col-md-2">
							<a href="index.html"><img src="images/logo.png" alt="" /></a>
						</div>
						<div class="col-md-10">
							<div class="letraLogo">Comida Rapida</div>
						</div>
					</div>					
				</div>		
				<div class="clearfix"></div>
			</div>

				<div class="clearfix"></div>
			</div>
			<div class="reviews-section">
				<h3 class="head">Descripción de comidas</h3>
					<div class="col-md-9 reviews-grids">
						@foreach($productos as $producto)
						<div class="review">
							<div class="movie-pic">
								<a href="single.html"><img src="product/{{$producto->path}}" alt="" /></a>
							</div>
							<div class="review-info">
								<a class="span" href="single.html"><i>{{$producto->name}}</i></a>
								<p class="dirctr">Comida Rapida de maracaibo estado Zulia.</p>				
								<p class="info"><b>DESCRIPCIÓN:</b> {{$producto->descripcion}}</p>
								<p class="info"><b>PRECIO:</b> {{$producto->precio}}</p>
								
							</div>
							<div class="clearfix"></div>
						</div>

						@endforeach
						{!!$productos->render()!!}

						
					</div>
					<div class="col-md-3 side-bar">					
					</div>

					<div class="clearfix"></div>
			</div>
			</div>
		<div class="review-slider">
			 <ul id="flexiselDemo1">
			<li><img src="images/r1.jpg" alt=""/></li>
			<li><img src="images/r2.jpg" alt=""/></li>
			<li><img src="images/r3.jpg" alt=""/></li>
			<li><img src="images/r4.jpg" alt=""/></li>
			<li><img src="images/r5.jpg" alt=""/></li>
			<li><img src="images/r6.jpg" alt=""/></li>
		</ul>
			<script type="text/javascript">
		$(window).load(function() {
			
		  $("#flexiselDemo1").flexisel({
				visibleItems: 6,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: false,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
					portrait: { 
						changePoint:480,
						visibleItems: 2
					}, 
					landscape: { 
						changePoint:640,
						visibleItems: 3
					},
					tablet: { 
						changePoint:768,
						visibleItems: 3
					}
				}
			});
			});
		</script>
		<script type="text/javascript" src="js/jquery.flexisel.js"></script>	
		

@stop