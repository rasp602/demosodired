@extends('layouts.admin')
{{-- 	@include('alert.success') --}}
	@section('content')
	<h1>Lista de Productos</h1>
		<table class="table">
			<thead>
				<th>Nombres</th>
				<th>Descripción</th>
				<th>Precio</th>
				<th>imagen</th>
				<th>Operaciones</th>
			</thead>
			@foreach($productos as $producto)
			<tbody>
				<td>{{$producto->name}}</td>
				<td>{{$producto->descripcion}}</td>
				<td>{{$producto->precio}}</td>
				<td>
					<img src="product/{{$producto->path}}" alt="" style="width:30px;">
				</td>
				<td>Editar</td>
			</tbody>
				@endforeach
		</table>
		{!!$productos->render()!!}
		
	@endsection
