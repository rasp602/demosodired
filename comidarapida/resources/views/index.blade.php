@extends('layouts.principal')
@section('content')
@include('alerts.errors')
@include('alerts.request')

<div class="header">
			<div class="top-header">
				<div class="logo">
					<div class="row">
						<div class="col-md-2">
							<a href="index.html"><img src="images/logo.png" alt="" /></a>
						</div>
						<div class="col-md-10">
							<div class="letraLogo">Comida Rapida</div>
						</div>
					</div>					
				</div>		
				<div class="clearfix"></div>
			</div>

				<div class="header-info">
					<div class="row">
						<div class="col-md-12 letraInicio"><h1>Inicia Sesión</h1></div>
						<div class="col-md-2"></div>
						<div class="col-md-8">				
							{!!Form::open(['route'=>'log.store', 'method'=>'POST'])!!}
							<div class="form-group">
								{!!Form::label('correo','Correo:')!!}	
								{!!Form::email('email',null,['class'=>'form-control', 'placeholder'=>'Ingresa tu correo'])!!}
							</div>
							<div class="form-group">
								{!!Form::label('contrasena','Contraseña:')!!}	
								{!!Form::password('password',['class'=>'form-control', 'placeholder'=>'Ingresa tu contraseña'])!!}
							</div>
							<div align="center">{!!Form::submit('Iniciar',['class'=>'btn btn-primary'])!!}<br><br></div>
							
							{!!Form::close()!!}</div>

						<div class="col-md-2"></div>
					</div>
	
				</div>
		
		</div>
		<div class="review-slider"><a href="comidas">
			 <ul id="flexiselDemo1">
			<li><img src="images/r1.jpg" alt=""/ class="img-circle"></li>
			<li><img src="images/r2.jpg" alt=""/ class="img-circle"></li>
			<li><img src="images/r3.jpg" alt=""/ class="img-circle"></li>
			<li><img src="images/r4.jpg" alt=""/ class="img-circle"></li>
			<li><img src="images/r5.jpg" alt=""/ class="img-circle"></li>
			<li><img src="images/r6.jpg" alt=""/ class="img-circle"></li>
		</ul>

		<script type="text/javascript" src="js/jquery.flexisel.js"></script>	
		</a></div>

		<div class="news">
			<div class="col-md-6 news-left-grid">
				<h3>Comidas rapidas venezolanas a tu gusto,</h3>
				<h2>Registrate ahora y puedes pedir tu comida online!</h2>
				<h4>Facil y rapido.</h4>
				<a href="registro"><i class="book"></i>REGISTRARSE</a>
				<p>Obtén un descuento del  <strong>10%</strong> si te registras ahora!</p>
			</div>
			<div class="col-md-6 news-right-grid">
				<h3>Bienvenidos</h3>
				<div class="news-grid">
					<h5>Este es el lugar de comida más importante de pais</h5>
				
					<p>Somos una venta de comida venezolanas, dedicados a complacer los paladares y gustos de nuestros compatriotas.</p>
				</div>
				<div class="news-grid">
					<h5>Nuestra meta es que nuestros clientes se sientan complacidos con nuestras comidas y servicios</h5>
				
					<p>Registrate online y puedes pedir tus comidas desde la comodida de tu hogar, tambien puedes darnos tu opinión y detalles de como te gustan las comidas para implementar nuevo platos a tu gusto!!! </p>
				</div>
			
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="more-reviews">


	<script type="text/javascript">
		$(window).load(function() {
			
		  $("#flexiselDemo2").flexisel({
				visibleItems: 4,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: false,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
					portrait: { 
						changePoint:480,
						visibleItems: 2
					}, 
					landscape: { 
						changePoint:640,
						visibleItems: 3
					},
					tablet: { 
						changePoint:768,
						visibleItems: 3
					}
				}
			});
			});
		</script>
		<script type="text/javascript" src="js/jquery.flexisel.js"></script>

					<script type="text/javascript">
		$(window).load(function() {
			
		  $("#flexiselDemo1").flexisel({
				visibleItems: 6,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: false,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
					portrait: { 
						changePoint:480,
						visibleItems: 2
					}, 
					landscape: { 
						changePoint:640,
						visibleItems: 3
					},
					tablet: { 
						changePoint:768,
						visibleItems: 4
					}
				}
			});
			});
		</script>
	@stop